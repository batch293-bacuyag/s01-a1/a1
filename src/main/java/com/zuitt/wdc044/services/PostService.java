package com.zuitt.wdc044.services;

import com.zuitt.wdc044.models.Post;
import org.springframework.http.ResponseEntity;

public interface PostService {

    //create a post method
    void createPost(String stringToken, Post post);

    //for getting all posts
    Iterable<Post> getPosts();

    //edit a user's post method
    ResponseEntity updatePost(Long id, String stringToken, Post post);

    //Delete a user's post method
    ResponseEntity deletePost(Long id, String stringToken);

    //get all user's post
    Iterable<Post> getUserPosts(String stringToken);
}
