package com.zuitt.wdc044;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

//This file serves as our index/entry point

//SpringBootApplication this is called as "Annotation" mark
//represented by the @ symbol
//This is a shortcut or the method we manually create or describe our purpose or configure a framework
//Annotation are used to provide supplemental information about the program

@SpringBootApplication

//tells the SpringBoot that this handle
@RestController
//to handle endpoints and methods

public class Wdc044Application {
	//this Method start the whole SpringBoot Framework
	public static void main(String[] args) {

		SpringApplication.run(Wdc044Application.class, args);
	}
	//how to run out application?
	//./mvnw spring-boot:run

	//mapping http get request
	@GetMapping("/hello")

	//Access query parameter

	//RequestParam is used to extract query parameters, form parameters, and even files from the request
	//name = Cardo: Hello Cardo
	//name = "", Helo World`
	//localhost:8080/hello?name=Cardo
	//"?" the start of the parameters followed by the "key=Value" pair


	public String hello(@RequestParam(value = "name", defaultValue = "World")String name){
		return String.format("Hello %s", name);
	}

	@GetMapping("/greetings")
	public String greetings(@RequestParam(value = "greet", defaultValue = "World")String greet){
		return String.format("Good evening, %s! Welcome to Batch 293", greet);
	}

	@GetMapping("/hi")
	public String hi(@RequestParam(value = "user", defaultValue = "user") String user) {
		return String.format("hi %s!", user);
	}

	@GetMapping("/nameage")
	public String nameAge(@RequestParam(value = "name", defaultValue = "user") String name,
						  @RequestParam(value = "age", defaultValue = "null") int age) {
		return String.format("Hello %s! Your age is %d.", name, age);
	}

}
